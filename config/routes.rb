Rails.application.routes.draw do
  devise_for :users, controllers:{
  	omniauth_callbacks: "users/omniauth_callbacks"
  }

  post "/custom_sign_up", to: "users/omniauth_callbacks#custom_sign_up"


  authenticated :user do
    root 'main#Home'
  end

  unauthenticated :user do
    root 'main#unregistered'
  end

  #1 mandar peticion a facebook 
  #2 facebook retorna respuesta al callback_url
  #3 procesar informacion de facebook



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
